---
title: Listen
authorbox: false
sidebar: false
menu: main
---

## SHOWS ARE LIVE AND THEN PODCASTED FOR NIGHTLY REBROADCAST ON SCHOOL DAYS.

WDOT has moved to a podcast format! Music used is incidental or background clips, in accordance of 37 CFR 380.2. of the US Copyright Law. None of our podcasts are permitted to contain music.

Our feed can be heard closed-circuit on campus on 530 AM

Please check out our shows by clicking on "Shows" on the navbar.

For special events, we will continue to use our direct internet stream:

We recommend VLC Media Player

[Outside the school](http://www2.portnet.org/portradio/wdotoutside.pls)

[Inside the school](http://www2.portnet.org/portradio/wdotinside.pls)
