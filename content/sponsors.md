---
title: Sponsors
authorbox: false
sidebar: false
menu: main
---

Updates coming soon! If you are interested in underwriting a show on WDOT, please email jklaff@portnet.k12.ny.us ... 30 dollars a semester, or $50 for the year, gets daily airtime, and your business card on our website.
