---
title: Times
authorbox: false
sidebar: false
menu: main
---

## SHOWS ARE LIVE AND THEN TAPED FOR NIGHTLY REBROADCAST
Shows are recorded during off periods, and/or before school.

See Mr. Klaff if you would like a timeslot! Shows are then put on-demand on the show page.

Shows must edit out their music before being put on-demand.

See show page for WDOT content.
